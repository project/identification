********************************************************************
                I D E N T I F I C A T I O N    M O D U L E
********************************************************************

DESCRIPTION:

   This module allows users to login with a custom identification
   number (or string). To configure this module, the site builder
   creates a (or specifies a pre-existing) field on the user entity.
   The value stored in this field for a particular user can
   then be used as the identification login credential.

********************************************************************

INSTALLATION:

1. Enable the module.
2. Go to admin/config/people/identification and select field and
   click save configuration. If fields not listed in the select box then
   click 'Create new field' link and create a field.
3. Now, User can login by using Identification values stored on the
   user entities.
